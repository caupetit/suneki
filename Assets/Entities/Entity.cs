﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Entity contain tools to generate a max number of prefab automatically in map.
/// </summary>
public class Entity
{
    /// <summary>
    /// Number of entities able to be created in a map.
    /// </summary>
    public int          Max;

    /// <summary>
    /// Currently Created object in a map
    /// </summary>
    public int          CreatedNb;

    /// <summary>
    /// The prefab to create.
    /// </summary>
    public Transform    Prefab;

    /// <summary>
    /// Initializes a new instance of the Entity class.
    /// </summary>
    /// <param name="max">Max number of prefab to create on map.</param>
    /// <param name="Prefab">Prefab to create.</param>
    public Entity(int max, Transform Prefab)
    {
        CreatedNb = 0;
        Max = max;
        this.Prefab = Prefab;
    }

    /// <summary>
    /// Create entities as much as needed in Entity param.
    /// </summary>
    /// <param name="entity">Instance of Entity class</param>
    /// <param name="Box">The bounds of the box you want to create objects. See BoxBounds script.</param>
    public static void Generate(Entity entity, BoxBounds Box)
    {
        int MaxGenerations = 0;

        while (entity.CreatedNb < entity.Max && MaxGenerations < 3000)
        {
            Vector3 Spawn = GetRandSpawn(Box);
            if (!Physics.CheckSphere(Spawn, 10f))
            {
                var instance = MonoBehaviour.Instantiate(entity.Prefab, Spawn, Quaternion.identity);
                entity.CreatedNb++;
                instance.name = entity.Prefab.name + entity.CreatedNb;
            }
            MaxGenerations++;
        }
    }

    /// <summary>
    /// Get a randim point in the bounds given.
    /// </summary>
    /// <param name="Box">The bounds of the box you want to create objects. See BoxBounds script.</param>
    /// <returns>Vector3 containing spawn positions</returns>
    private static Vector3 GetRandSpawn(BoxBounds Box)
    {
        Vector3 Spawn;

        if (Box == null)
            return Vector3.zero;
        Spawn = new Vector3(Random.Range(Box.MinRange.x, Box.MaxRange.x),
            Random.Range(Box.MinRange.y, Box.MaxRange.y),
            Random.Range(Box.MinRange.z, Box.MaxRange.z));
        return Spawn;
    }

    /// <summary>
    /// Reset entity values for new map generation.
    /// </summary>
    public virtual void Reset()
    {
        Max = 1;
        CreatedNb = 0;
    }
}
