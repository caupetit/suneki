﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

/// <summary>
/// Builds tail animation.
/// Must be attached to a Snake.
/// </summary>
public class Tail : MonoBehaviour {

	enum _Curves
	{
		posX,
		posY,
		posZ,
		rotX,
		rotY,
		rotZ
	};

    /// <summary>
    /// Animation virtual time.
    /// </summary>
    public float animationTime = 0f;

    /// <summary>
    /// Number of chunks to be build.
    /// </summary>
    public int chunksQueue = 3;

    // Time the last chunk was created.
    private float lastChunkTime = 0f;

    // Time since the last FixedUpdate.
    private float refreshTimer;

    // Describe snake moves.
    private AnimationCurve[] Curves;

    /// <summary>
    /// Contains the Snake moves.
    /// </summary>
	public AnimationClip clip;

    /// <summary>
    /// Describes a tail chunk. Must be attached to the script.
    /// </summary>
    public Transform prefab;

    /// <summary>
    /// Snake size.
    /// </summary>
    public int size;

    /// <summary>
    /// Offset between tail chunks.
    /// </summary>
    public const float chunkOffset = 12f;

    // Snake movement script.
    private move moveScript;

    private PlayerController player;

    /// <summary>
    /// Time offset between tail chunks.
    /// </summary>
    public float chunkTimeOffset
    {
        get
        {
            return (chunkOffset / move.baseSpeed);
        }
    }

    /// <summary>
    /// Grows the snake.
    /// </summary>
    public void Grow()
    {
        Transform chunk;
        size += 1;
        chunk = Instantiate(prefab) as Transform;
        chunk.name = "Chunk" + this.name;
    }

	void Awake () {
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        refreshTimer = 0f;
        size = 0;
		clip = new AnimationClip();
        clip.name = "Test";
        clip.wrapMode = WrapMode.Once;
        Curves = new AnimationCurve[7];
        Curves[0] = new AnimationCurve(new Keyframe(0, transform.localPosition.x));
        Curves[1] = new AnimationCurve(new Keyframe(0, transform.localPosition.y));
        Curves[2] = new AnimationCurve(new Keyframe(0, transform.localPosition.z));
        Curves[3] = new AnimationCurve(new Keyframe(0, transform.localRotation.x));
        Curves[4] = new AnimationCurve(new Keyframe(0, transform.localRotation.y));
        Curves[5] = new AnimationCurve(new Keyframe(0, transform.localRotation.z));
        Curves[6] = new AnimationCurve(new Keyframe(0, transform.localRotation.w));
        clip.SetCurve("", typeof(Transform), "localPosition.x", Curves[0]);
        clip.SetCurve("", typeof(Transform), "localPosition.y", Curves[1]);
        clip.SetCurve("", typeof(Transform), "localPosition.z", Curves[2]);
        clip.SetCurve("", typeof(Transform), "localRotation.x", Curves[3]);
        clip.SetCurve("", typeof(Transform), "localRotation.y", Curves[4]);
        clip.SetCurve("", typeof(Transform), "localRotation.z", Curves[5]);
        clip.SetCurve("", typeof(Transform), "localRotation.w", Curves[6]);
        moveScript = this.GetComponent<move>();
    }

	void FixedUpdate ()
    {
        if (player.isAlive)
            buildCurves();
        chunksGenerator();
    }

    // Build and set curves.
    private void buildCurves()
    {
        float animeDeltaTime;

        if (refreshTimer >= chunkTimeOffset / 2)
        {
            refreshTimer -= chunkTimeOffset / 2;
            Curves[0].AddKey(animationTime, transform.localPosition.x);
            Curves[1].AddKey(animationTime, transform.localPosition.y);
            Curves[2].AddKey(animationTime, transform.localPosition.z);
            Curves[3].AddKey(animationTime, transform.localRotation.x);
            Curves[4].AddKey(animationTime, transform.localRotation.y);
            Curves[5].AddKey(animationTime, transform.localRotation.z);
            Curves[6].AddKey(animationTime, transform.localRotation.w);
            clip.SetCurve("", typeof(Transform), "localPosition.x", Curves[0]);
            clip.SetCurve("", typeof(Transform), "localPosition.y", Curves[1]);
            clip.SetCurve("", typeof(Transform), "localPosition.z", Curves[2]);
            clip.SetCurve("", typeof(Transform), "localRotation.x", Curves[3]);
            clip.SetCurve("", typeof(Transform), "localRotation.y", Curves[4]);
            clip.SetCurve("", typeof(Transform), "localRotation.z", Curves[5]);
            clip.SetCurve("", typeof(Transform), "localRotation.w", Curves[6]);
        }
        animeDeltaTime = Time.deltaTime * moveScript.speedCoef;
        animationTime += animeDeltaTime;
        refreshTimer += animeDeltaTime;
    }

    // Creates chunks if needed and possible.
    private void chunksGenerator()
    {
        if (chunksQueue > 0)
        {
            if (lastChunkTime + chunkTimeOffset <= animationTime)
            {
                Grow();
                lastChunkTime = animationTime;
                chunksQueue--;
            }
        }
    }
}
