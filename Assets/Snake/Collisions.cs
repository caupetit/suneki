﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Add it to the Snake gameobject.
/// Manage collisions for death or eat.
/// </summary>
public class Collisions : MonoBehaviour
{
    // Script containing all entities data.    
    PlayerController player;

    // food Entity. see FoodGenerator.
    private FoodGenerator foodGenerator;

    // script managing tails.
    Tail scriptTail;

    // Get all scripts needed.
    void Start()
    {
        foodGenerator = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<FoodGenerator>();
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        scriptTail = this.GetComponent<Tail>();
    }

    // Manage Collisions on snake head.
    // Kill or makes eat.
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == Tags.food)
            EatAction(col);
        else
            player.Die();
    }

    // Makes player eat, update food generation
    // and add a tailchunk to the snake.
    private void EatAction(Collider col)
    {
        Destroy(col.gameObject);
        foodGenerator.UpdateGeneration();
        player.Eat();
        scriptTail.chunksQueue += 2;
    }
}
